import { RedisClient } from 'redis';
import { setupConf } from '../setup-conf';
import { ClientAuth } from '../client-auth';

const argv = require('minimist')(process.argv.slice(2));

if (!argv['name'] || argv.length > 1) {
    console.error('Usage: create-client.ts --name <client name>');
    process.exit(1);
}

try {
    const config = setupConf();
    config.required(['isbn:redis']);

    const redis = new RedisClient(config.get('isbn:redis'));

    const auth = new ClientAuth(config, redis);
    auth.init();

    auth.createClient(argv['name'])
    .then(id => console.log('Client created with id: ' + id))
    .then(() => process.exit(0))
    .catch(err => {
        console.error(err);
        process.exit(1);
    });
} catch (err) {
    console.error(err);
    process.exit(1);
}
