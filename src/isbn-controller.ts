import * as express from 'express';
import * as async from 'async';
import { Provider } from 'nconf';
import { Transporter } from 'nodemailer';
import { RedisClient } from 'redis';

import { ClientAuth } from './client-auth';

const listKey = 'isbn:unused';
const usedListKey = 'isbn:used';

// from https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s13.html
function isValidISBN(isbn: string): boolean {
    const regex = new RegExp(/^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/);

    if (regex.test(isbn)) {
        // Remove non ISBN digits, then split into an array
        const chars = isbn.replace(/[- ]|^ISBN(?:-1[03])?:?/g, "").split("");
        // Remove the final ISBN digit from `chars`, and assign it to `last`
        const last = chars.pop();
        let sum = 0;
        let check, i;

        if (chars.length === 9) {
            // Compute the ISBN-10 check digit
            chars.reverse();
            for (i = 0; i < chars.length; i++) {
                sum += (i + 2) * parseInt(chars[i], 10);
            }
            check = 11 - (sum % 11);
            if (check === 10) {
                check = "X";
            } else if (check === 11) {
                check = "0";
            }
        } else {
            // Compute the ISBN-13 check digit
            for (i = 0; i < chars.length; i++) {
                sum += (i % 2 * 2 + 1) * parseInt(chars[i], 10);
            }
            check = 10 - (sum % 10);
            if (check === 10) {
                check = "0";
            }
        }

        if (check.toString() === last) {
            return true
        } else {
            return false
        }
    } else {
        return false;
    }
}

function normalizeISBN(isbn: string) {
    return isbn.replace(/[- ]|^ISBN(?:-1[03])?:?/g, "");
}

function getNumbers(redis: RedisClient) {
    return (req, res, next) => {
        redis.smembers(listKey, (err, members) => {
            if (err) {
                return next(Error('Database error'));
            }

            res.set('X-Total-Count', members.length.toString());

            res.json(members);
        });
    };
}

function ensureCanPut(auth: ClientAuth) {
    return (req, res, next) => {
        if (auth.clientCanAddNumbers(req)) {
            return next();
        }
        return next({ message: 'Unauthorized', status: 401 });
    };
}

function convertCSV(req, res, next) {
    if (req.get('Content-Type') === 'text/csv') {
        req.body = req.body.split(/\r?\n/).filter(item => item.length);
    }
    next();
}

function ensureValidInput(req, res, next) {
    try {
        if (!(req.body instanceof Array)) {
            throw { message: 'ISBN numbers should be submitted as array of strings', status: 400 };
        }

        req.body.forEach(isbn => {
            if (typeof isbn !== 'string') {
                throw { message: 'ISBN numbers must be strings', status: 400 };
            }

            if (!isValidISBN(isbn)) {
                throw { message: 'Invalid ISBN', status: 400 };
            }
        });
        next();
    } catch (err) {
        return next(err);
    }
}

function ensureNotAddingUsedNumbers(redis) {
    return (req, res, next) => {
        async.each(req.body,
            (isbn, done) => {
                const number = normalizeISBN(isbn);
                redis.sismember(usedListKey, number, (err, isMember) => {
                    if (err) {
                        done(Error('Database error'));
                    } else if (isMember) {
                        done({message: 'Number ' + number + ' has already been used', status: 409});
                    } else {
                        done()
                    }
                })
            }, next);
    };
}

function putNumberList(redis: RedisClient) {
    return (req, res, next) => {
        async.each(req.body,
            (isbn, done) => {
                // strip away dashes and prefixes to avoid duplication
                const number = normalizeISBN(isbn);
                redis.sadd(listKey, number, done);
            },
            err => {
                if (err) {
                    return next(Error('Database error'));
                }

                res.json({ message: 'accepted' });
            },
        );
    }
}

function popNumber(redis: RedisClient) {
    return (req, res, next) => {
        redis.spop(listKey, (err, value) => {
            if (err) {
                return next(Error('Database error'));
            }

            if (!value) {
                return next({ message: 'List empty!', status: 404 });
            }

            redis.sadd(usedListKey, value, err => {
                if (err) {
                    return next(err);
                }

                res.json({isbn: value});
            });

            next();
        });
    };
}

function warnIfTooFewNumbersLeft(redis: RedisClient, transporter: Transporter, emailConfig: any) {
    return (req, res, next) => {
        // If email has been setup, check how many unused numbers are left, and send email if fewer than threshold.
        if (transporter) {
            redis.scard(listKey, (err, cardinality) => {
                if (err) {
                    // don't want to send this error to client
                    return console.error(Error('Database error'));
                }

                if (cardinality < emailConfig.whenFewerThan) {
                    transporter.sendMail({
                        from: emailConfig.from,
                        to: emailConfig.to,
                        subject: `ISBN list only has ${cardinality} numbers left`,
                        text: 'Upload new numbers as soon as possible.'
                    }, (err, info) => {
                        if (err) {
                            // We don't want to send email errors to clients
                            console.error('Error sending email:', err);
                        }
                        if (info) {
                            console.log(`INFO: Only ${cardinality} unused numbers left, notification sent with envelope ${JSON.stringify(info.envelope)}`);
                        }
                        next();
                    });
                }
            });
        }
    };
}

function releaseISBN(redis) {
    return (req, res, next) => {
        const isbn = normalizeISBN(req.params.isbn);
        redis.sismember(usedListKey, isbn, (err, ismember) => {
            if (err) {
                return next(Error('Database error'));
            }

            if (!ismember) {
                return next({ message: 'ISBN ' + req.params.isbn + ' is not in the used numbers list', status: 404 });
            }

            redis.smove(usedListKey, listKey, isbn, err => {
                if (err) {
                    return next(err);
                }

                res.json({ message: 'ISBN ' + req.params.isbn + ' released' });
            });
        });
    };
}

export function ISBNController(config: Provider, redis: RedisClient, auth: ClientAuth, transporter?: Transporter) {
    const router = express.Router();

    const emailConfig = config.get('isbn:email');

    // get all available numbers, count in header
    router.get('/', getNumbers(redis));

    // Add more ISBN numbers to the list of unused numbers
    router.put('/', ensureCanPut(auth), convertCSV, ensureValidInput, ensureNotAddingUsedNumbers(redis), putNumberList(redis));

    // reserve an ISBN number
    router.post('/reserve', popNumber(redis), warnIfTooFewNumbersLeft(redis, transporter, emailConfig));

    // if an ISBN that has been reserved is not to be used it can be release
    router.post('/release/:isbn', releaseISBN(redis));

    return router;
}
